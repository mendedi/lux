**Requirements**
Comment-out "proxy" variables if any at the end of ~/.bashrc. For teams to work properly.

**Prepare**
```
cd /home/devlin/
python3 -m venv venv
pip3 install vpn-slice
. ./venv/bin/activate
vpn-slice --self-test
deactivate
sudo cp vpnconnect.sh.edited /urs/bin/.
```

**Run**
```
/usr/bin/vpnconnect.sh.edited
```

**SSH through a proxy**
```
ssh USER@FINAL_DEST -o "ProxyCommand=nc -X connect -x PROXYHOST:PROXYPORT %h %p"
```

Alternative into `$HOME/.ssh/config`
```
Host myserver
  User martin
  ProxyCommand nc -X 5 -x 127.0.0.1:9052 %h %p

 # to run
 #$ ssh myserver
```

