## how to map with netbox

```
 # collect services to expose. e.g.:
10.67.5.147:6060 , netbox
10.67.5.147:1661 , netbox-backup-agent

>> netbox-dns >> find your internal netbox to map, e.g.:
[INTERNAL] ec.local	netbox-cmdb	10.67.5.147
(it might need to updated and wait 5 min. If still not working at the end.)

 # netbox-dns to create domain names and link to IPs
>> netbox-dns >> records >> clone some that already point to rps (as c4hou), with `netbox`
as name >> save. e.g:
[EXTERNAL] dke.tech.ec.europa.eu 	netbox	52.208.0.188

 # rps to link externals with internals
>> reverse proxy >> mappings >> add e.g.:
source: https://netbox.dke.tech.ec.europa.eu dest: http://netbox-cmdb.ec.local
source: https://netbox.dke.tech.ec.europa.eu dest: http://netbox-cmdb.ec.local:8080

>> certificates >> certificates >> add >> e.g.:
CN: netbox.dke.tech.ec.europa.eu 	Let's Encrypt

```
