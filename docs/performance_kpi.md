# KPI

##### Possible tools to measure separated or together.

- Django
- Docker
- Redis
- Postgres

##### Possible resources to keep track of.

- Memory
- CPU

##### API ends to test.

- GET
- POST
- PUT
- DELETE

##### Base request.

Create some fixed requests for different API end points to test.  
Separate those requests on different groups.

Some possible groups:
- GET, requests.
- POST, requests.
- PUT, requests.
- DELETE, requests.
- Request use JOINs internally.
- Request use "Group by" internally.
- Request use "UNION" internally.
- Simple requests.


### Questions to answer

##### First question to solve.

Quality of service.

- What are our limits?

Probably 4 cores, and 4GB memory could be a good reference to start tests.  
The smaller the resources, the bigger the room for extension we will have.


##### Performance, questions to answer.

- CPU usage per tool.
- Allocated memory per tool.
- Total CPU usage.
- Total allocated memory.
- Execution time per request.
- Avg execution time per request in a group.

##### Overloading tests.

Create some script to be able to execute a different amount of request  
repetitively in the same way.

- How many calls can handle?
- Number of requests.
- Number of succeeded requests.
- Number of lost request.
- Number of failed request.

#####

API test? import test? bulk CSV import test?
apache beanchmark
