### Possible Flowchart.

```mermaid
flowchart TB
    A[Start] --> B
    B(1. To collect:
    -Expected requirements and performance, 
    -Customer feedback,
    -Known bottlenecks.)
    B --> C
    C(Service capacity and performance analysis)
    C --> C2(Goals && scope)
    C2 -->D(Plan && Design)
    D --> E
    E[Prototipe version]
    E --> E1(Test)
    E --> E2(fa:fa-book Document)
    E --> E3(Clean Code)
    E1 --> F
    E2 --> F
    E3 --> F
    F(Analize result,
    Report results)
    F -- not good --> D
    F --> G(Develop Branch)
    G --> H(Goal reached?
    Request fulfilment?)
    H -- no -->C2
    H -- yes --> J1(Inform costumer)
    J1 -->J2(Review old and new Documentation.
    Looking for changes.)
    J2 -->J3(Production Branch)
```
