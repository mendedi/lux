

To Install locally Netbox dev environment, check official README from rps-plugin repo.
-------

Commands, examples
-------

```
git clone git@code.europa.eu:digit-c4/netbox/netbox-rps-plugin.git
cd netbox-rps-plugin
mkdir -p lib
git clone --branch v3.6.9 https://github.com/netbox-community/netbox.git lib/netbox
```

Copy and edit ".env" file
```
cp .env.example .env
```

```bash
python3 -m venv venv
echo "set -a; . .env >/dev/null 2>&1 || . .env.example; set +a" >> venv/bin/activate
. venv/bin/activate
python3 -m pip install --upgrade pip setuptools wheel
pip install -r lib/netbox/requirements.txt
pip3 install poetry
poetry install
```

Start
```
docker compose up -d
```

Migrate
```
netbox-nms migrate
netbox-nms createsuperuser
netbox-nms runserver 0.0.0.0:8000 --insecure
```

Visit http://localhost:8000/  
You might need to modify `netbox/netbox/netbox/configuration.py` file if connecting from outside.

[Official docs from netbox-rps-plugin repo](netbox-rps-plugin/README.md)



Tests. Remove `--keepdb` if you switch plugin.
```
netbox-nms test tests --keepdb -v 2
```

How to migrate up and down a plugin:
```
netbox-nms migrate netbox_rps_plugin
netbox-nms migrate netbox_rps_plugin 0019
netbox-nms migrate netbox_rps_plugin zero
```

Docs:
```
apt-get install -y graphviz graphviz-dev
netbox-nms graph_models --pydot  netbox_rps_plugin -o docs/model/schema_diagram.svg
```

Others
-------

CLI to connect to postgres:
```
psql -h localhost -p 5432 -U netbox -d netbox -W
secret
set search_path to public;
\d
```

CLI to connect to redis:
```
 # example with pass, pass might not be needed.
redis-cli -h localhost -a J5brHrAXFLQSif0K
```
