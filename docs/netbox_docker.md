# Official docs

[https://github.com/netbox-community/netbox-docker](https://github.com/netbox-community/netbox-docker?tab=readme-ov-file)

```
git clone -b release https://github.com/netbox-community/netbox-docker.git
cd netbox-docker
tee docker-compose.override.yml <<EOF
version: '3.4'
services:
  netbox:
    ports:
      - 8001:8080
EOF
docker compose pull
docker compose up
```

To create the first admin user run this command:
```
docker compose exec netbox /opt/netbox/netbox/manage.py createsuperuser
docker compose up
docker compose start
```

If you want to just stop NetBox to continue work later on, use the following command.
```
 # Stop all the containers
docker compose stop

 # Start the containers again
docker compose start
```
If you want to stop NetBox and clean up any resources it allocated (database, files, etc.), use the following command. Attention: It will remove any data you have entered in NetBox!
```
docker compose down -v
```
