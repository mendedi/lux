### Ansible related

**To install Ansible**

```bash
pip3 install ansible
pip3 install ansible-lint	# if needed
```

**Troubleshooting**

Might need to install some ansible modules:
```
ansible-galaxy collection install -r requirements.yml
pip3 install pynetbox jmespath
```

**To run, example commands**

```
 # VAR1=somevar2pass ansible-playbook -i <mylab.com>, <playbook_to_run>.yml -e "ansible_user=debian var2=some"
ansible-playbook -i <mylab.com>, start_and_stop.yml -e "ansible_user=debian"
```

**Back-up playbook, example**

```
scp /tmp/netbox.sql mendedi@mylab:/tmp/.
ansible-playbook -i inventory.yaml backup_restore/restore.yml --extra-vars "@backup_restore/vars/dev.json"
```
