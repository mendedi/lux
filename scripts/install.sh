#!/bin/sh

read -p "Your user name? " choice
# systemd-service
#  soft links
#  script to execute and service
ln -s /home/"$choice"/lux/scripts/netbox-daemon.sh /usr/local/bin/netbox-daemon.sh
cp scripts/netbox-daemon.service /lib/systemd/system/netbox-daemon.service
systemctl daemon-reload
systemctl enable netbox-daemon.service
echo "\nTo run netbox can be done by:\nsystemctl start netbox-daemon"
